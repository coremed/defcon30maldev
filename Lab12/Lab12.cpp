#include <iostream>
#include <windows.h>


// mouse detection
int mouse_active() {

    POINT position1, position2;
    GetCursorPos(&position1);
    Sleep(5000);
    GetCursorPos(&position2);

    if ((position1.x == position2.x) && (position1.y == position2.y)) {
        return 0;
    }
    else {
        return 1;
    }
}

int main() {

    int moved = mouse_active();
    printf("Mouse moved: %i", moved);
}

