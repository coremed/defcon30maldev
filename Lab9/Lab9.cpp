#include <iostream>

using namespace std;
#include <windows.h>
#include <string>
#include <chrono>
using namespace std::chrono;
#include <thread>
#include <stdio.h>
#include <stdlib.h>
#include <TlHelp32.h>
#include <WinUser.h>
#include <Psapi.h>

// Is prime?
bool IsItPrime(ULONGLONG n) {

	if (n <= 1) {
		return false;
	}
	if (n <= 3) {
		return true;
	}
	if (n % 2 == 0 || n % 3 == 0) {
		return false;
	}
	for (ULONGLONG i = 5; i * i <= n; i = i + 6) {

		if (n % i == 0 || n % (i + 2)) {
			return false;
		}
	}
	return true;
}

int TickSleepCheck() {
	
	DWORD tick1 = GetTickCount();
	Sleep(1000);
	if (GetTickCount() - tick1 > 980) {
		return 1;
	}
	else {
		return 0;
	}
}

int main() {

	// is prime debug
	bool isPrime;
	isPrime = IsItPrime(11);
	printf("%s\n", isPrime ? "true" : "false");

	// sleep detection debug
	auto start = high_resolution_clock::now();
	Sleep(2000);
	auto stop = high_resolution_clock::now();
	auto duration = duration_cast<milliseconds>(stop - start);
	unsigned int dwDuration = duration.count();
	// anti-debug check
	if (dwDuration < 2000) {
		return 0;
	}
	printf("Slept for: %i\n",dwDuration);

	// get tick count debug
	int tickCheck = TickSleepCheck();
	printf("Tick Check: %i\n", tickCheck);
}

