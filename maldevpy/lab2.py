#Lab 2

from base64 import encode
import sys

offset = 42

def format(string, every=60):
	line = []
	line.append("unsigned char shit = ")
	for i in range(0, len(string), every):
		line.append("\"" + string[i:i + every] + "\"")
	return '\n'.join(line)

def cc(shellfile):
	c = 1
	b = ""
	enc_shellcode = ""
	
	step1 = shellfile.replace("unsigned char buf[] = ", "")
	step2 = step1.replace("\n", "")
	step3 = step2.replace(";", "")
	step4 = step3.replace("\\", "")
	step5 = step4.replace('"', "")
	enc_hex = step5.replace("x", "")
	
	for i in enc_hex:
		if(c % 2 == 0):
			b = b + i
			b = int(b, 16)
			b = int(b + offset)
			b = int(b % 256)
			b = hex(b)[2:]
			if(len(b) == 1):
				b = '0' + b
			b = "\\x" + b
			enc_shellcode = enc_shellcode + b
			b = ""
		else: 
			b = b + i
		c += 1
	enc_shellcode = enc_shellcode
	return enc_shellcode

def main():
	try:
		shellfile = open(sys.argv[1], "r").read()
	except:
		print("file arg needed")
		sys.exit()
	
	enc_text = cc(shellfile)
	print(format(enc_text))
	
if __name__ == "__main__":
    main()
	
