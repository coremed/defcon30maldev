import sys
import itertools

KEY = "w+Hc"

def xortxt(shellfile, key):
	n = 2
	b = ""
	enc_scode = ""
	
	try:
		step1 = shellfile.replace("unsigned char buf[] = ", "")
		step2 = step1.replace("\n", "")
		step3 = step2.replace('"', "")
		step4 = step3.replace("\\", "")
		step5 = step4.replace(";", "")
		scode_hex = step5.replace("x", "")
	except:
		scode_hex = shellfile
	
	for x,y in zip(range(0, len(scode_hex), n), itertools.cycle(range(0, len(key)))):
		b = scode_hex[x:x + n]
		decbyte = int(b, 16)
		kb = key[y % len(key)]
		enc = decbyte ^ ord(kb)
		hexenc = hex(enc)[2:]
		if(len(hexenc) == 1):
			hexenc = "0" + hexenc
		format1 = "\\x" + hexenc
		enc_scode += format1
	return enc_scode
	
def format_txt(string, every=60):
	lines = []
	lines.append("unsigned char defconwifi[] = ")
	for i in range(0, len(string), every):
		lines.append("\"" + string[i:i + every] + "\"")
	return '\n'.join(lines)

def main():
	try: 
		shellfile = open(sys.argv[1], "r").read()
	except:
		print("you fucked up")
		sys.exit()
	enc_txt = xortxt(shellfile, KEY)
	final = format_txt(enc_txt)
	print(final)
	
if __name__ == "__main__":
    main()
