import sys
import binascii

KEY = "w+Hc"

def xorbin(shellfile, key):
	b = ""
	enc_scode = ""
	
	for i in range(0,len(shellfile)):
		b = shellfile[i]
		kb = key[i % len(key)]
		enc = b ^ ord(kb)
		hexenc = hex(enc)[2:]
		if (len(hexenc) == 1):
			hexenc = "0" + hexenc
		enc_scode += hexenc
	format1 = enc_scode.encode()
	format2 = binascii.a2b_hex(format1)
	return format2
		

def main():
	try: 
		shellfile = open(sys.argv[1], "rb").read()
	except:
		print("you fucked up")
		sys.exit()
	enc_txt = xorbin(shellfile, KEY)
	name = sys.argv[1] + ".enc"
	open(name, "wb").write(enc_txt)
	
if __name__ == "__main__":
    main()
