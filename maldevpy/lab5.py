import sys
import binascii

offset = 42

def ccbin(shellfile):
	c = 1
	b = ""
	shellcode_hex = shellfile
	enc_shellcode = ""
	
	step1 = binascii.b2a_hex(shellfile)
	step2 = step1.decode()
	
	for i in step2:
		if(c % 2 == 0):
			b = b + i
			b = int(b, 16)
			b = int(b + offset)
			b = int(b % 256)
			b = hex(b)[2:]
			if(len(b) == 1):
				b = '0' + b
			enc_shellcode = enc_shellcode + b
			b = ""
		else: 
			b = b + i
		c += 1
		
		
	step3 = enc_shellcode.encode()
	step4 = binascii.a2b_hex(step3)
	return step4
	
	
def main():
	try:
		shellfile = open(sys.argv[1], "rb").read()
	except:
		print("No file")
		sys.exit()
	
	name = sys.argv[1] + ".enc"
	enc_txt = ccbin(shellfile)
	open(name, "wb").write(enc_txt)	
	
if __name__ == "__main__":
    main()
