#Lab 3

import sys


offset = 197

def ccstring(shellcode):
	c = 1
	b = ""
	shellcode_hex = shellcode
	enc_shellcode = ""
	
	for i in shellcode_hex:
		if (c % 2 == 0):
			b = b + i
			b = int(b, 16)
			b = int(b + offset)
			b = int(b % 256)
			b = hex(b)[2:]
			if(len(b) == 1):
				b = '0' + b
			b = "\\x" + b
			enc_shellcode = enc_shellcode + b
			b = ""
		else: 
			b = b + i
		c += 1
	enc_shellcode = enc_shellcode
	return enc_shellcode

def string2hex(s):
	ptext = s.encode('utf-8')
	hexstr = ptext.hex()
	return hexstr

def main():
	sval = sys.argv[1]
	enctxt = string2hex(sval)
	cc_txt = ccstring(enctxt)
	print(cc_txt)
	
if __name__ == "__main__":
    main()
